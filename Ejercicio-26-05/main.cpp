#include <SFML\Network\Http.hpp>
#include <json.hpp>
#include <iostream>
void main()
{
	std::cout << "Ingrese la ciudad: ";
	std::string city;
	std::cin >> city;
	sf::Http http("http://query.yahooapis.com");
	sf::Http::Request request;
	request.setUri("/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + city + "%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");
	sf::Http::Response response = http.sendRequest(request);

	nlohmann::json jdata = nlohmann::json::parse(response.getBody().c_str());

	std::cout << jdata["query"]["results"]["channel"]["item"]["condition"]["text"];

	std::cin.get();
	std::cin.get();
}